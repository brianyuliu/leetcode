# coin change 
def coinChange(coins: list, amount: int) -> int:
        coins = sorted(coins)
        counter = 0
        while coins:
            curr = coins.pop()
            while amount - curr >= 0:
                amount-=curr
                counter+=1
        print(amount)
        if amount:
            return -1
        return counter
    
if __name__ == "__main__":
    print(coinChange([1,2,5], 11)) # should print 3
    print(coinChange([186,419,83,408], 6249)) # should print 20
    
